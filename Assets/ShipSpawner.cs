﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Lukas Moungos
 */

public class ShipSpawner : MonoBehaviour
{
    public GameObject obj;
    public float timer;
    public float oldTimer;

    private void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            GameObject @object = Instantiate(obj, transform.position, transform.rotation);
            @object.GetComponent<Rigidbody>().AddForce(transform.forward * 40, ForceMode.VelocityChange);
            @object.transform.localScale = new Vector3(2, 2, 2);
            @object.GetComponent<destroyMe>().enabled = true;
            @object.GetComponentInChildren<Starfield>().enabled = false;
            timer = oldTimer;
        }
    }
}