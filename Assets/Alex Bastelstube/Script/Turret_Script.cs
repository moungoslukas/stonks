﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Alexander Herrmann
 */

public class Turret_Script : MonoBehaviour
{
    #region Public Fields

    public float fireRate = 1f;
    private float fireCountdown = 0f;
    public float spread = 0f;
    public int range = 20;
    public GameObject bulletPrefab;
    public Transform firePoint;
    private bool IsAbleToShoot = true;

    #endregion Public Fields

    #region Unity Methods

    private Transform target;
    public Transform partToRotate;
    public string enemyTag = "Target";
    private Random random = new Random();

    public float turnSpeed = 10f;

    // Start is called before the first frame update
    private void Start()
    {
        SetTarget();
    }

    // Update is called once per frame
    private void Update()
    {
        if (target == null)
            return;
        if (target.transform.position.z - transform.position.z > range)
            return;
        if (IsAbleToShoot)
        {
            IsShooting();
        }
    }

    private void IsShooting()
    {
        partToRotate.LookAt(target);
        if (partToRotate.rotation.y > 0.5 || partToRotate.rotation.y < -0.5)
        {
            IsAbleToShoot = false;
        }
        if (fireCountdown <= 0f)
        {
            Shoot();
            fireCountdown = 60 / fireRate;
        }
        fireCountdown -= Time.deltaTime;
    }

    #endregion Unity Methods

    #region Private Methods

    private void SetTarget()
    {
        GameObject enemy = GameObject.FindGameObjectWithTag(enemyTag);
        target = enemy.transform;
    }

    #endregion Private Methods

    private void Shoot()
    {
        var randX = Random.Range(-spread, spread);
        var randY = Random.Range(-spread, spread);
        var randZ = Random.Range(-spread, spread);
        GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        bulletGO.transform.Rotate(randX, randY, randZ);
        Rocket bullet = bulletGO.GetComponent<Rocket>();
        if (bullet != null)
        {
            bullet.Seek(target);
        }
    }
}