﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Alexander Herrmann
 */

public class Bullet_Test : MonoBehaviour
{
    #region Public Fields

    public float projectileSpeed = 50f;
    public static int damage = 10;
    public bool is_seeking = false;

    #endregion Public Fields

    #region Private Fields

    public Transform target;

    #endregion Private Fields

    #region Unity Methods

    // Update is called once per frame
    private void Update()
    {
        if (target == null) return;
        Vector3 dir = target.position - transform.position;

        float distanceThisFrame = projectileSpeed * Time.deltaTime;
        transform.Translate(0, 0, projectileSpeed * Time.deltaTime);

        if (dir.z <= 20) is_seeking = false;

        if (is_seeking == true)
        {
            transform.LookAt(target);
        }
    }

    #endregion Unity Methods

    #region Private Methods

    public void Seek(Transform _target)
    {
        target = _target;
    }

    #endregion Private Methods
}