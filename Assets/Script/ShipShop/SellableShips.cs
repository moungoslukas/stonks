﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/* 
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Alexander Herrmann
 */

public class SellableShips : MonoBehaviour
{

    public Dictionary<GameObject, ShipData> shipDictionary = new Dictionary<GameObject, ShipData>();
    private void Start()
    {

        List<GameObject> shipList = GameObject.FindGameObjectsWithTag("SellableShip").ToList();
        foreach(var s in shipList)
        {   
            ShipPrice shipPrice = s.GetComponent<ShipPrice>();
            bool isPurchased = PlayerPrefs.GetInt(s.name) == 0 ? false : true;
            shipPrice.isPurchased = isPurchased;
            ShipData shipData = new ShipData(shipPrice.price, isPurchased , s);
            shipDictionary[s] = shipData;
        }
    }
}

public struct ShipData
{
    public readonly GameObject ship;
    public readonly int price;
    public readonly bool isPurchased;
    public ShipData( int price, bool isPurchased, GameObject ship)
    {
        this.price = price;
        this.isPurchased = isPurchased;
        this.ship = ship;
    }
}