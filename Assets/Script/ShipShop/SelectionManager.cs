﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/* 
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Alexander Herrmann
 */

public class SelectionManager : MonoBehaviour
{
    #region Public Fields
    public GameObject camera;
    public Vector3 currentLoc;
    public int destination = 0;
    public GameObject textPrice;
    public GameObject buttonPurchase;
    public GameObject imageCoins;
    public Text text;
    public GameObject button;
    #endregion

    #region Private Fields
    private int currentIndex;
    private float speed = 2.5f;
    private ShipManager shipManager;

    #endregion

    #region Unity Methods
 
    // Start is called before the first frame update
    void Start()
    {
        shipManager = FindObjectOfType<ShipManager>();
        destination = 0;
    }

    public void ButtonLeft()
    {
        if (currentIndex == shipManager.shipList.Count -1)
        {
            return;
        }
        destination += 30;
        shipManager.OnShipChange(destination);
        text.text = shipManager.Price().ToString();
    }
    public void ButtonRight()
    {
        if (currentIndex == 0)
        {
            return;
        }
        destination -= 30;
        shipManager.OnShipChange(destination);
        text.text = shipManager.Price().ToString();
    }

    public void Select()
    {
        if (shipManager.IsPurchased() == false)
        {
            return;
        }
        SceneManager.LoadScene(1);
        shipManager.OnSelect();
    }

    public void Buy()
    {
        int coins = PlayerPrefs.GetInt("Coins");
        int price = shipManager.Price();
        if (price <= coins)
        {
            PlayerPrefs.SetInt("Ship" + currentIndex, 1);
            PlayerPrefs.SetInt("Coins", coins - price);
            shipManager.Purchased();
        }
        else
        {
            return;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (shipManager.IsPurchased() == true)
        {
            textPrice.SetActive(false);
            buttonPurchase.SetActive(false);
            imageCoins.SetActive(false);
            button.SetActive(true);
        }
        else
        {
            textPrice.SetActive(true);
            buttonPurchase.SetActive(true);
            imageCoins.SetActive(true);
            button.SetActive(false);
        }
        currentIndex = destination / 30;    
        camera.transform.position = Vector3.Lerp(camera.transform.position, new Vector3(0f, 0f, destination), speed * Time.deltaTime);
        currentLoc = camera.transform.position;      
    }
    
    #endregion
 
    #region Private Methods
    
    #endregion
  
}