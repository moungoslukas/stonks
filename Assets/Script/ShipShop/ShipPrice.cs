﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* 
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Alexander Herrmann
 */

public class ShipPrice : MonoBehaviour
{
    public int price;
    public bool isPurchased;
}