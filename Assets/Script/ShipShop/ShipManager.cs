﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/* 
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Alexander Herrmann
 */

public class ShipManager : MonoBehaviour
{
    #region Public Fields
    public GameObject currentShip;
    #endregion

    #region Private Fields
    private SelectionManager selectionManager;
    [HideInInspector] public List<GameObject> shipList;
    private SellableShips sellableShips;
    #endregion

    #region Unity Methods
    private void Start()
    {
        sellableShips = FindObjectOfType<SellableShips>();
        shipList = GameObject.FindGameObjectsWithTag("SellableShip").ToList();
        selectionManager = FindObjectOfType<SelectionManager>();
    }
    public bool IsPurchased()
    {
        return sellableShips.shipDictionary[currentShip].isPurchased;       
    }
    public int Price()
    {
        return sellableShips.shipDictionary[currentShip].price;
    }

    public void Purchased()
    {
        sellableShips.shipDictionary[currentShip] = new ShipData(Price(), true, currentShip);
        PlayerPrefs.SetInt(currentShip.name, 1);
        currentShip.GetComponent<ShipPrice>().isPurchased = true;
    }

    public void OnShipChange(float zCord)
    {
        currentShip = shipList.Where(s => s.transform.position.z == zCord).FirstOrDefault();
    }
    public void OnSelect()
    {
        PlayerPrefs.SetString("currentShip", currentShip.name);
    }

    #endregion Unity Methods

}