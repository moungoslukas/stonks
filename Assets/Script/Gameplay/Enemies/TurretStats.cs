﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Lukas Moungos
 */

public class TurretStats : MonoBehaviour, IEnemyStats
{
    public int _health { get => health; set => health = value; }
    [SerializeField] private int health = 100;
    public int _scoreValue { get => scoreValue; set => scoreValue = value; }
    [SerializeField] private int scoreValue = 250;
    public int _coinValue { get => coinValue; set => coinValue = value; }
    [SerializeField] private int coinValue = 1;
    public float _pickupDropChance { get => pickupDropChance; set => pickupDropChance = value; }

    [Space(10)]
    [SerializeField, Range(0, 1)] private float pickupDropChance = 0.5f;

    public GameObject partToDestroy;

    [System.Serializable]
    public class Pickup
    {
        public GameObject prefab;
    }

    public Pickup[] pickups;

    private PlayerStats pStats;
    private ParticleManager particleManager;

    private void Start()
    {
        pStats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        particleManager = FindObjectOfType<ParticleManager>();
    }

    private void Update()
    {
        if (_health <= 0)
        {
            float chance = 1 - Random.value;
            if (chance < _pickupDropChance)
            {
                Instantiate(pickups[0].prefab, new Vector3(transform.position.x, transform.position.y + 3, transform.position.z), transform.rotation);
            }
            pStats.coins += _coinValue;
            pStats.score += _scoreValue;
            particleManager.CreateParticleOnPoint(partToDestroy.transform.position, transform.rotation, ParticleType.EnergyExplosion);
            Destroy(partToDestroy);
            gameObject.GetComponent<BoxCollider>().enabled = false;
            gameObject.GetComponent<StationaryTurret>().enabled = false;
            (gameObject.GetComponent<IEnemyStats>() as MonoBehaviour).enabled = false;
        }
    }
}