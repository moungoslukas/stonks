﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Lukas Moungos
 */

public class BossStats : MonoBehaviour, IEnemyStats
{
    public int _health { get => health; set => health = value; }
    [SerializeField] private int health = 25000;
    private float startHealth;
    public int _scoreValue { get => scoreValue; set => scoreValue = value; }
    [SerializeField] private int scoreValue = 250;
    public int _coinValue { get => coinValue; set => coinValue = value; }
    [SerializeField] private int coinValue = 1;
    public float _pickupDropChance { get => pickupDropChance; set => pickupDropChance = value; }

    [Space(10)]
    [SerializeField, Range(0, 1)] private float pickupDropChance = 1f;

    public GameObject pickup;
    public GameObject turret;
    public float spawnTurretRange;
    private PlayerStats pStats;
    private ParticleManager particleManager;
    private ObjectSpawner objectSpawner;
    public Image healthBar;

    private GameObject player;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        pStats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        particleManager = FindObjectOfType<ParticleManager>();
        objectSpawner = FindObjectOfType<ObjectSpawner>();
        objectSpawner.spawnEnemies = false;
        startHealth = _health;
    }

    private GameObject lastObject;

    private void Update()
    {
       
        healthBar.fillAmount = (float)_health / startHealth;     
        transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, transform.position.y, player.transform.position.z - 95), Time.deltaTime * 2);
        if (_health <= 0)
        {
            Instantiate(pickup, new Vector3(transform.position.x, transform.position.y, transform.position.z), transform.rotation);
            pStats.coins += _coinValue;
            pStats.score += _scoreValue;
            particleManager.CreateParticleOnPoint(transform.position, transform.rotation, ParticleType.BossExplosion);
            objectSpawner.spawnEnemies = true;
            Destroy(this.gameObject);
        }
        else
        {
            if (lastObject != null && lastObject.transform.position.z - (transform.position.z - 230) < spawnTurretRange) { return; }
            GameObject obj = Instantiate(turret);
            obj.transform.position = new Vector3(3.5f, transform.position.y - 2, transform.position.z - 230);
            obj = Instantiate(turret);
            obj.transform.position = new Vector3(-3.5f, transform.position.y - 2, transform.position.z - 230);
            lastObject = obj;
        }
    }
}