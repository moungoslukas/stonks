using System.Collections;
using System.Collections.Generic;
using UnityEngine;

internal interface IPickup
{
    string name { get; }

    int value { get; }

    //GameObject WeaponPrefab { get;}
    void GiveAttribute(GameObject obj);
}