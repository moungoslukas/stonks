﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Alexander Herrmann
 */

public interface IEnemyStats
{
    int _health { get; set; }
    int _scoreValue { get; set; }
    float _pickupDropChance { get; set; }
    int _coinValue { get; set; }
}