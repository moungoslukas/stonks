using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coins : MonoBehaviour, IPickup
{
    public string name => "Coins";
    public int value => 5;

    public void GiveAttribute(GameObject obj)
    {
        obj.GetComponent<PlayerStats>().coins += value;
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
            FindObjectOfType<AudioManger>().Play("CoinPickup");
        }
        gameObject.GetComponent<SpawnEffect>().enabled = true;
    }
}