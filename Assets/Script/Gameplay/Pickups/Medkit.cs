using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Medkit : MonoBehaviour, IPickup
{
    public string name => "Medkit";
    public int value => 5;

    public void GiveAttribute(GameObject obj)
    {
        PlayerStats pickupMnager = obj.GetComponent<PlayerStats>();
        pickupMnager.health += value;
        if (pickupMnager.health > 100)
        {
            pickupMnager.health = 100;
        }
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
        FindObjectOfType<AudioManger>().Play("PowerUp");
        gameObject.GetComponent<SpawnEffect>().enabled = true;
    }
}