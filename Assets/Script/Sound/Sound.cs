﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

/* 
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Alexander Herrmann
 */
[System.Serializable]
public class Sound 
{
    public string name;

    public AudioClip clip;

    public bool loop;

    [Range(0f,1f)]
    public float volume;
    [Range(0f, 2f)]
    public float pitch;
    [HideInInspector]
    public AudioSource source;
    
}