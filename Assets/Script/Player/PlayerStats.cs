using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerStats : MonoBehaviour
{
    public int coins;
    public int score;
    public int health = 100;
    public float ScoreTimeIntervall;
    public int ScorePerIntervall;
    public GameObject gameOverUI;
    public GameObject statsUI;

    [System.Serializable]
    public class Shield
    {
        public int armorPoints = 100;
        public float shieldRegenCooldown = 6;
        public float regenTime = 0.2f;
        public int regenRate = 2;
        [HideInInspector] public float lastHitTime;
        [HideInInspector] public int ArmorPointsAtPointOfRechargeStart;
    }

    public Shield shield;
    private ParticleManager particleManager;
    private AudioManger audioManger;
    private bool ScoreSubmitted = false;
    private bool CoinsSubmitted = false;
    private float TimeOfNextAction = 0f;
    private float IngameTime;
    private float TimePassed;

    private void Start()
    {
        particleManager = FindObjectOfType<ParticleManager>();
        audioManger = FindObjectOfType<AudioManger>();
        TimePassed = Time.time;
        InvokeRepeating("Timer", 0, Time.deltaTime);
    }

    private void OnEnable()
    {
        Time.timeScale = 1;
    }

    public void Timer()
    {
        IngameTime = Time.time - TimePassed;
    }

    private void Update()
    {
        if (IngameTime > TimeOfNextAction && SceneManager.GetActiveScene().buildIndex == 2 && health >0)
        {
            TimeOfNextAction += ScoreTimeIntervall;
            score += ScorePerIntervall;
        }
        shield.lastHitTime -= Time.deltaTime;
        if (shield.lastHitTime <= 0 && shield.armorPoints != 100 && health > 0 && !CR_Running)
        {
            StartCoroutine("ShieldRegen");
        }
        if (health <= 0)
        {
            if (ScoreSubmitted == false && score > PlayerPrefs.GetInt("Highscore"))
            {
                PlayerPrefs.SetInt("Highscore", score);
                ScoreSubmitted = true;
            }
            if (CoinsSubmitted == false)
            {
                int newCoins = PlayerPrefs.GetInt("Coins") + coins;
                PlayerPrefs.SetInt("Coins", newCoins);
                CoinsSubmitted = true;
            }
            if (Time.timeScale <= 1 && Time.timeScale > 0.99)
            {
                audioManger.Play("GameOver");
            }
            if (Time.timeScale < 0.4 && Time.timeScale > 0.2)
                particleManager.CreateParticleOnPoint(transform, ParticleType.BigEnergyExplosion);
            audioManger.SubSound("Theme", Time.deltaTime * 0.04f);
            Time.timeScale -= Time.deltaTime * 0.5f;
            gameOverUI.SetActive(true);
            statsUI.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        GameObject @object = other.gameObject;
        if (@object.tag == "PickupItem")
        {
            @object.GetComponent<IPickup>().GiveAttribute(gameObject);
        }
        IProjectile projectile = other.gameObject.GetComponent<IProjectile>();
        if (@object.tag == "Bullet" || @object.tag == "Missile")
        {
            if (@object.tag == "Missile")
            {
                audioManger.Play("Explosion");
            }
            shield.lastHitTime = shield.shieldRegenCooldown;
            if (shield.armorPoints == 0)
            {
                health -= projectile._damage;
                if (@object.tag == "Bullet")
                    particleManager.CreateParticleOnParent(transform, other.ClosestPoint(@object.transform.position), ParticleType.RedLaserImpact);
                else
                    particleManager.CreateParticleOnPoint(@object.transform, ParticleType.Explosion);
            }
            else
            {
                shield.armorPoints -= projectile._damage;
                if (shield.armorPoints <= 0)
                {
                    shield.armorPoints = 0;
                    audioManger.Play("ShieldEmpty");
                }
                if (@object.tag == "Bullet")
                    particleManager.CreateParticleOnParent(transform, other.ClosestPoint(@object.transform.position), ParticleType.RedLaserImpact);
                else
                    particleManager.CreateParticleOnPoint(@object.transform, ParticleType.Explosion);
            }
            Destroy(other.gameObject);
        }
    }

    private bool CR_Running = false;

    private IEnumerator ShieldRegen()
    {
        CR_Running = true;
        audioManger.Stop("ShieldEmpty");
        if (shield.armorPoints > 0)
            audioManger.Play("Overshield");
        else
            audioManger.Play("ShieldRecharging");
        do
        {
            shield.armorPoints += shield.regenRate;
            if (shield.armorPoints > 100) shield.armorPoints = 100;
            yield return new WaitForSeconds(shield.regenTime);
        }
        while (shield.armorPoints < 100 && shield.lastHitTime <= 0);

        audioManger.Stop("ShieldRecharging");
        CR_Running = false;
        yield return null;
    }
}