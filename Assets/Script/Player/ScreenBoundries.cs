﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Lukas Moungos
 */

public class ScreenBoundries : MonoBehaviour
{
    #region Unity Methods

    // Update is called after all Update functions have been called.
    private void Update()
    {
        Vector3 viewPosition = transform.position;
        viewPosition.x = Mathf.Clamp(viewPosition.x, -4.5f, 4.5f);
        transform.position = viewPosition; //setzt die Position des Objekts gleich mit dem Vector3 viewPosition damit das gesetzte Bewegungslimit auf das Objekt übertragen wird
    }

    #endregion Unity Methods
}