﻿using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Lukas Moungos
 */

public class PlayerController : MonoBehaviour
{
    #region Public Fields

    public PlayerInputController inputController;
    public GameObject playerModel;
    public Vector3 velocity = Vector3.zero;

    #endregion Public Fields

    #region Private Fields

    [SerializeField] [Range(0f, 50f)] private float movementSpeed = 0.25f;
    [SerializeField] [Range(0f, 50f)] private float flyingSpeed = 0.25f;
    private Turret_MkI turret;
    private Vector2 moveDelta;
    private float fireCountdown = 0;
    private bool isTouched = false;
    private Vector3 lastVelocity = Vector3.zero;

    private PlayerInput Input
    {
        get
        {
            return inputController.inputActions;
        }
    }

    private AudioManger audioManger;

    #endregion Private Fields

    #region Unity Methods

    private void Awake()
    {
        Input.Controls.Movement.performed += ctx => moveDelta = ctx.ReadValue<Vector2>();
        Input.Controls.OnPress.started += _ => isTouched = true;
        Input.Controls.OnPress.canceled += _ => isTouched = false;
    }

    // Start is called before the first frame update
    private void Start()
    {
        GameObject WeaponPrefab = Instantiate(Resources.Load("Weapons/Turrets/MkI/Weapon") as GameObject);
        WeaponPrefab.transform.parent = playerModel.transform;
        WeaponPrefab.transform.localPosition = new Vector3(0, -0.09f, -0.3f);
        turret = WeaponPrefab.GetComponent<Turret_MkI>();
        audioManger = FindObjectOfType<AudioManger>();
        transform.Find(PlayerPrefs.GetString("currentShip")).gameObject.SetActive(true);
    }

    // Update is called once per frame

    private void Update()
    {
        Vector3 mouse = new Vector3(moveDelta.x, moveDelta.y, 25);
        Vector3 pos = Camera.main.ScreenToWorldPoint(mouse);

        transform.Translate(Vector3.forward * Time.deltaTime * flyingSpeed);

        velocity = (transform.position - lastVelocity) / Time.deltaTime;
        lastVelocity = transform.position;

        if (!isTouched) return;
        float transformX = Mathf.Clamp(pos.x, -3.5f, 3.5f);
        float transformY = Mathf.Clamp(pos.y, 5f, 10f);
        if (transformY > 8.5)
            transformY = .15f * transformY;
        else if (transformY < 6.5)
            transformY = -0.6f;
        else
            transformY = 0;

        transform.position = Vector3.Lerp(transform.position, new Vector3(transformX, 6.75f, transform.position.z - transformY), movementSpeed * Time.deltaTime);

        if (fireCountdown <= 0)
        {
            audioManger.Play("PlayerShot");
            turret.Shoot();
            fireCountdown = 60 / turret.rpm;
        }
        fireCountdown -= Time.deltaTime;
    }

    #endregion Unity Methods
}