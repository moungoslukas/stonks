﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Lukas Moungos
 */

public interface IWeapon
{
    string _name { get; set; }
    GameObject _target { get; set; }
    Transform _partToRotateYAxis { get; set; }
    Transform _partToRotateXAxis { get; set; }

    void Shoot();
}

public interface ITurret
{
    float _roundsPerMinute { get; set; }
    float _turnSpeed { get; set; }
    float _spread { get; set; }
    int _range { get; set; }
    int _minRange { get; set; }
    GameObject _projectilePrefab { get; }
    Transform[] _firePoints { get; }
}