﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Lukas Moungos
 */

public interface IProjectile
{
    int _damage { get; set; }
    float _velocity { get; set; }
    bool _playerProjectile { get; set; }
}