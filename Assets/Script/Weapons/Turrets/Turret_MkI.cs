﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Lukas Moungos
 */

public class Turret_MkI : MonoBehaviour, IWeapon
{
    public string _name { get => weaponName; set => weaponName = value; }
    [SerializeField] private string weaponName;
    public GameObject _target { get => target; set => target = value; }
    [SerializeField, HideInInspector] private GameObject target;
    public Transform _partToRotateYAxis { get => partToRotateYAxis; set => partToRotateYAxis = value; }
    [SerializeField] private Transform partToRotateYAxis;

    public Transform _partToRotateXAxis { get => partToRotateXAxis; set => partToRotateYAxis = value; }
    [SerializeField] private Transform partToRotateXAxis;

    [System.Serializable]
    public class Turret : ITurret
    {
        public float _roundsPerMinute { get => roundsPerMinute; set => roundsPerMinute = value; }
        [SerializeField] private float roundsPerMinute;
        public float _turnSpeed { get => turnSpeed; set => turnSpeed = value; }
        [SerializeField] private float turnSpeed;
        public float _spread { get => spread; set => spread = value; }
        [SerializeField] private float spread;
        public int _range { get => range; set => range = value; }
        [SerializeField] private int range;
        public int _minRange { get => minRange; set => minRange = value; }
        [SerializeField] private int minRange;
        public GameObject _projectilePrefab { get => projectilePrefab; }
        [SerializeField] private GameObject projectilePrefab;
        public Transform[] _firePoints { get => firePoints; }
        [SerializeField] private Transform[] firePoints;
    }

    public Turret turret;

    [HideInInspector] public float rpm { get { return turret._roundsPerMinute; } }

    public void Shoot()
    {
        foreach (var hole in turret._firePoints)
        {
            GameObject bullet = Instantiate(turret._projectilePrefab);
            bullet.transform.position = hole.transform.position;
        }
    }
}