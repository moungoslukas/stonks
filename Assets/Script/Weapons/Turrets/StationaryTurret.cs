﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Alexander Herrmann
 */

public class StationaryTurret : MonoBehaviour, IWeapon
{
    public string _name { get => weaponName; set => weaponName = value; }
    [SerializeField] private string weaponName;
    public GameObject _target { get => target; set => target = value; }
    [SerializeField] private GameObject target;
    public Transform _partToRotateYAxis { get => partToRotateYAxis; set => partToRotateYAxis = value; }
    [SerializeField] private Transform partToRotateYAxis;

    public Transform _partToRotateXAxis { get => partToRotateXAxis; set => partToRotateYAxis = value; }
    [SerializeField] private Transform partToRotateXAxis;

    [System.Serializable]
    public class Turret : ITurret
    {
        public float _roundsPerMinute { get => roundsPerMinute; set => roundsPerMinute = value; }
        [SerializeField] private float roundsPerMinute;
        public float _turnSpeed { get => turnSpeed; set => turnSpeed = value; }
        [SerializeField] private float turnSpeed;
        public float _spread { get => spread; set => spread = value; }
        [SerializeField] private float spread;
        public int _range { get => range; set => range = value; }
        [SerializeField] private int range;
        public int _minRange { get => minRange; set => minRange = value; }
        [SerializeField] private int minRange;
        public GameObject _projectilePrefab { get => projectilePrefab; }
        [SerializeField] private GameObject projectilePrefab;
        public Transform[] _firePoints { get => firePoints; }
        [SerializeField] private Transform[] firePoints;
    }

    public Turret turret;

    [HideInInspector] public float rpm { get { return turret._roundsPerMinute; } }
    private float fireCountdown = 0;

    private ParticleManager particleManager;

    public void Shoot()
    {
        var randX = Random.Range(-turret._spread, turret._spread);
        var randY = Random.Range(-turret._spread, turret._spread);
        var randZ = Random.Range(-turret._spread, turret._spread);
        foreach (var point in turret._firePoints)
        {
            particleManager.CreateParticleOnParent(point.transform, ParticleType.RedLaserFlash);
            GameObject projectile = (GameObject)Instantiate(turret._projectilePrefab, point.position, Quaternion.LookRotation(point.forward));
            projectile.transform.Rotate(randX, randY, randZ);
            var missile = projectile.GetComponent<Rocket>();
            if (missile != null) missile.Seek(_target.transform);
        }
    }

    private void Start()
    {
        GameObject enemy = GameObject.FindGameObjectWithTag("Player");
        _target = enemy;

        fireCountdown = Random.Range(0.2f, 1.2f);
        particleManager = FindObjectOfType<ParticleManager>();
    }

    private void Update()
    {
        if (target == null)
            return;
        if (target.transform.position.z - transform.position.z > turret._range)
            return;
        if (target.transform.position.z - transform.position.z <= turret._minRange)
            (gameObject.GetComponent<IWeapon>() as MonoBehaviour).enabled = false;

        _partToRotateYAxis.LookAt(new Vector3(_target.transform.position.x, 0, _target.transform.position.z));
        _partToRotateXAxis.LookAt(_target.transform.position);

        if (fireCountdown <= 0f)
        {
            Shoot();
            fireCountdown = 60 / rpm;
        }
        fireCountdown -= Time.deltaTime;
    }
}