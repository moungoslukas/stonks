﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Lukas Moungos
 */

public class Boss : MonoBehaviour, IWeapon
{
    public string _name { get => weaponName; set => weaponName = value; }
    [SerializeField] private string weaponName;
    public GameObject _target { get => target; set => target = value; }
    [SerializeField] private GameObject target;
    public Transform _partToRotateYAxis { get => partToRotateYAxis; set => partToRotateYAxis = value; }
    [SerializeField] private Transform partToRotateYAxis;

    public Transform _partToRotateXAxis { get => partToRotateXAxis; set => partToRotateYAxis = value; }
    [SerializeField] private Transform partToRotateXAxis;

    [System.Serializable]
    public class Turret : ITurret
    {
        public bool _secondary;
        public float _secondaryIntervall;
        public float _roundsPerMinute { get => roundsPerMinute; set => roundsPerMinute = value; }
        [SerializeField] private float roundsPerMinute;
        public float _secondaryRoundsPerMinute;
        public float _turnSpeed { get => turnSpeed; set => turnSpeed = value; }
        [SerializeField] private float turnSpeed;
        public float _spread { get => spread; set => spread = value; }
        [SerializeField] private float spread;
        public int _range { get => range; set => range = value; }
        [SerializeField] private int range;
        public int _minRange { get => minRange; set => minRange = value; }
        [SerializeField] private int minRange;
        public GameObject _projectilePrefab { get => projectilePrefab; }
        [SerializeField] private GameObject projectilePrefab;
        public GameObject _secondaryProjectilePrefab;
        public Transform[] _firePoints { get => firePoints; }
        [SerializeField] private Transform[] firePoints;
    }

    public Turret turret;

    [HideInInspector] public float rpm { get { return turret._roundsPerMinute; } }
    [HideInInspector] public float rpmSecondary { get { return turret._secondaryRoundsPerMinute; } }
    [HideInInspector] public bool secondary { get { return turret._secondary; } set { turret._secondary = value; } }
    [HideInInspector] public float secondaryIntervall { get { return turret._secondaryIntervall; } }
    private float secondaryTimer;

    private float fireCountdown = 0;

    private ParticleManager particleManager;

    public void Shoot()
    {
        if (secondary) { StartCoroutine("SpecialShot"); return; }

        var randX = Random.Range(-turret._spread, turret._spread);
        var randY = Random.Range(-turret._spread, turret._spread);
        var randZ = Random.Range(-turret._spread, turret._spread);
        for (int i = 6; i < turret._firePoints.Length; i++)
        {
            var point = turret._firePoints[i];
            particleManager.CreateParticleOnParent(point.transform, ParticleType.RedLaserFlash);
            GameObject projectile = (GameObject)Instantiate(turret._projectilePrefab, point.position, Quaternion.LookRotation(point.forward));
            projectile.transform.Rotate(randX, randY, randZ);
            var missile = projectile.GetComponent<Rocket>();
            if (missile != null) missile.Seek(_target.transform);
        }
    }

    private bool CR_Running = false;

    private IEnumerator SpecialShot()
    {
        CR_Running = true;
        int shots = 0;
        yield return new WaitForSeconds(3);
        do
        {
            var point = turret._firePoints[shots];
            particleManager.CreateParticleOnParent(point.transform, ParticleType.RedLaserFlash);
            GameObject projectile = (GameObject)Instantiate(turret._secondaryProjectilePrefab, point.position, Quaternion.LookRotation(point.forward));
            var missile = projectile.GetComponent<Rocket>();
            if (missile != null) missile.Seek(_target.transform);
            shots++;
            yield return new WaitForSeconds(60 / rpmSecondary);
        }
        while (shots < 6);

        CR_Running = false;
        secondary = false;
        secondaryTimer = secondaryIntervall;
        yield return null;
    }

    private void Start()
    {
        secondaryTimer = secondaryIntervall;
        GameObject enemy = GameObject.FindGameObjectWithTag("Player");
        _target = enemy;

        fireCountdown = Random.Range(0.2f, 1.2f);
        particleManager = FindObjectOfType<ParticleManager>();
    }

    private void Update()
    {
        if (target == null)
            return;
        if (target.transform.position.z - transform.position.z > turret._range)
            return;
        if (target.transform.position.z - transform.position.z <= turret._minRange)
            (gameObject.GetComponent<IWeapon>() as MonoBehaviour).enabled = false;

        turret._firePoints[6].LookAt(_target.transform.position);
        turret._firePoints[7].LookAt(_target.transform.position);

        if (secondaryTimer <= 0) secondary = true;
        secondaryTimer -= Time.deltaTime;

        if (fireCountdown <= 0f && !CR_Running)
        {
            Shoot();
            fireCountdown = 60 / rpm;
        }
        fireCountdown -= Time.deltaTime;
    }
}