﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Alexander Herrmann
 */

public class Rocket : MonoBehaviour, IProjectile
{
    public int _damage { get => damage; set => damage = value; }
    [SerializeField] private int damage;
    public float _velocity { get => velocity; set => velocity = value; }
    [SerializeField] private float velocity;
    public bool _playerProjectile { get => playerProjectile; set => playerProjectile = value; }
    [SerializeField] private bool playerProjectile = false;

    public bool seeking = true;

    private Transform target;
    private ParticleManager particleManager;
    private AudioManger audioManger;

    #region Unity Methods

    private void Start()
    {
        particleManager = FindObjectOfType<ParticleManager>();
        audioManger = FindObjectOfType<AudioManger>();
    }

    private void OnTriggerEnter(Collider other)
    {
        GameObject @object = other.gameObject;
        if (@object.tag == "Bullet" && @object.GetComponent<IProjectile>()._playerProjectile)
        {
            particleManager.CreateParticleOnPoint(@object.transform, ParticleType.SmallExplosion);
            audioManger.Play("Explosion");
            Destroy(@object);
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        if (target == null) return;
        Vector3 dir = target.position - transform.position;

        float distanceThisFrame = _velocity * Time.deltaTime;
        transform.Translate(0, 0, _velocity * Time.deltaTime);

        if (dir.z <= 20) seeking = false;

        if (seeking == true)
        {
            transform.LookAt(target);
        }
    }

    #endregion Unity Methods

    public void Seek(Transform target)
    {
        this.target = target;
    }
}