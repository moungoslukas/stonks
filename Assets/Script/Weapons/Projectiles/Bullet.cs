﻿using UnityEngine;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Lukas Moungos, Alexander Herrmann
 */

public class Bullet : MonoBehaviour, IProjectile
{
    public int _damage { get => damage; set => damage = value; }
    [SerializeField] private int damage;
    public float _velocity { get => velocity; set => velocity = value; }
    [SerializeField] private float velocity;
    public bool _playerProjectile { get => playerProjectile; set => playerProjectile = value; }
    [SerializeField] private bool playerProjectile = false;

    private ParticleManager particleManager;

    #region Unity Methods
    private void Update()
    {
        transform.Translate(transform.forward * _velocity * Time.deltaTime, Space.World);
    }

    // Start is called before the first frame update
    private void Start()
    {
        if (_playerProjectile) _velocity = -_velocity;
        particleManager = FindObjectOfType<ParticleManager>();
    }

    //OnCollisionEnter is called when this collider/rigidbody has begun touching another rigidbody/collider.
    private void OnCollisionEnter(Collision collision)
    {
        GameObject @object = collision.gameObject;

        if (@object.tag == "Player" && !_playerProjectile)
        {
            Destroy(gameObject);
            return;
        }
        if (@object.tag == "Enemy" && _playerProjectile)
        {
            @object.GetComponent<IEnemyStats>()._health -= _damage;

            particleManager.CreateParticleOnPoint(collision.contacts[0].point, transform.rotation, ParticleType.BulletImpact);
            Destroy(gameObject);
        }
        else if (@object.tag == "Barrier")
        {
            particleManager.CreateParticleOnParent(@object.transform, ParticleType.Sparks);
            Destroy(gameObject);
        }
    }

    #endregion Unity Methods
}