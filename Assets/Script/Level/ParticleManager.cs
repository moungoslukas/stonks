﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Lukas Moungos
 */

public class ParticleManager : MonoBehaviour
{
    [System.Serializable]
    public class Particle
    {
        public ParticleType type;
        public GameObject prefab;
    }

    public Particle[] particles;

    public void CreateParticleOnPoint(Vector3 v, Quaternion q, ParticleType type)
    {
        Particle particle = particles.Where(s => s.type == type).FirstOrDefault();
        Instantiate(particle.prefab, v, q);
    }

    public void CreateParticleOnPoint(Transform t, ParticleType type)
    {
        Particle particle = particles.Where(s => s.type == type).FirstOrDefault();
        Instantiate(particle.prefab, t.position, t.rotation);
    }

    public void CreateParticleOnParent(Transform parent, Vector3 v, ParticleType type)
    {
        Particle particle = particles.Where(s => s.type == type).FirstOrDefault();
        GameObject obj = Instantiate(particle.prefab, v, parent.rotation);
        obj.transform.SetParent(parent);
    }

    public void CreateParticleOnParent(Transform parent, ParticleType type)
    {
        Particle particle = particles.Where(s => s.type == type).FirstOrDefault();
        GameObject obj = Instantiate(particle.prefab, parent.position, parent.rotation);
        obj.transform.SetParent(parent);
    }
}

public enum ParticleType
{
    EnergyExplosion,
    Explosion,
    SmallExplosion,
    Sparks,
    RedLaserFlash,
    RedLaserImpact,
    PurpleRocketFlash,
    BulletImpact,
    BigEnergyExplosion,
    BossExplosion
}