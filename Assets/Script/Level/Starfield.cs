﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Lukas Moungos
 */

public class Starfield : MonoBehaviour
{
    #region Public Fields

    public int starsMax = 100;
    public float starSize = 1;
    public float starDistance = 10;
    public float starClipDistance = 5;
    public ParticleSystem pS;
    public StarFieldRender renderMethod;

    #endregion Public Fields

    #region Private Fields

    private Transform tx;
    private ParticleSystem.Particle[] points = null;
    private float starDistanceSqr => starDistance * starDistance;
    private float starClipDistanceSqr => starClipDistance * starClipDistance;

    #endregion Private Fields

    #region Unity Methods

    // Start is called before the first frame update
    private void Start()
    {
        tx = transform;
    }

    // Update is called once per frame
    private void Update()
    {
        if (points == null) CreateStarts();

        for (int i = 0; i < starsMax; i++)
        {
            if ((points[i].position - tx.position).sqrMagnitude > starDistanceSqr)
            {
                Vector2 starField = Random.insideUnitCircle;
                points[i].position = new Vector3(starField.x, 0, starField.y) * starDistance + tx.position;
            }

            if ((points[i].position - tx.position).sqrMagnitude <= starClipDistanceSqr)
            {
                float percentage = (points[i].position - tx.position).sqrMagnitude / starClipDistanceSqr;
                points[i].startColor = new Color(1, 1, 1, percentage);
                points[i].startSize = percentage * starSize;
            }

            pS.SetParticles(points, points.Length);
        }
    }

    // LateUpdate is called after all Update functions have been called.

    #endregion Unity Methods

    private void CreateStarts()
    {
        points = new ParticleSystem.Particle[starsMax];

        for (int i = 0; i < starsMax; i++)
        {
            if (renderMethod == StarFieldRender.Circle)
            {
                Vector2 starField = Random.insideUnitCircle;
                points[i].position = new Vector3(starField.x, 0, starField.y) * starDistance + tx.position;
            }
            else if (renderMethod == StarFieldRender.Sphere)
            {
                Vector3 starField = Random.insideUnitSphere;
                points[i].position = new Vector3(starField.x, starField.y, starField.z) * starDistance + tx.position;
            }

            points[i].startColor = new Color(1, 1.1f, 1);
            points[i].startSize = starSize;
        }
    }
}

public enum StarFieldRender
{
    Circle,
    Sphere
}