﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Lukas Moungos
 */

public class Scenery : MonoBehaviour
{
    #region Public Fields

    public Transform playerModel;

    #endregion Public Fields

    #region Unity Methods

    // Update is called once per frame
    private void Update()
    {
        transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, transform.position.y, playerModel.position.z - 200), Time.deltaTime * 10);
    }

    #endregion Unity Methods
}