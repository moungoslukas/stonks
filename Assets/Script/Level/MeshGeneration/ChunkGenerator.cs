﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Lukas Moungos
 */

public class ChunkGenerator : MonoBehaviour
{
    #region Public Fields

    public const float maxViewDistance = 300;
    public Transform viewer;
    public Material mapMatrial;
    public MapGenerator mapGeneratorObject;
    public static MapGenerator mapGenerator;
    public static Vector2 viewerPosition;

    #endregion Public Fields

    #region Private Fields

    private int chunkSize;
    private int chunkVisibleInViewDistance;

    private Dictionary<Vector2, MeshChunk> chunkDictionary = new Dictionary<Vector2, MeshChunk>();
    private List<MeshChunk> meshChunksVisibleLastUpdate = new List<MeshChunk>();

    #endregion Private Fields

    #region Unity Methods

    // Start is called before the first frame update
    private void Start()
    {
        mapGenerator = mapGeneratorObject;
        chunkSize = MapGenerator.chunkSize;
        chunkVisibleInViewDistance = Mathf.RoundToInt(maxViewDistance / chunkSize);
    }

    private void Update()
    {
        viewerPosition = new Vector2(viewer.position.x, viewer.position.z);
        UpdateVisibleChunks();
    }

    #endregion Unity Methods

    #region Private Methods

    private void UpdateVisibleChunks()
    {
        foreach (var chunk in meshChunksVisibleLastUpdate)
        {
            chunk.SetVisible(false);
        }
        meshChunksVisibleLastUpdate.Clear();

        int currentChunkCoordX = Mathf.RoundToInt(viewerPosition.x / chunkSize);
        int currentChunkCoordY = Mathf.RoundToInt(viewerPosition.y / chunkSize);
        for (int yOffset = -chunkVisibleInViewDistance; yOffset <= 1; yOffset++) // limitiert auf chunkVisibleInViewDistance + 1 Chunk nach hinten
        {
            for (int xOffset = -4; xOffset <= 4; xOffset++) //limitiert auf 8 Chunks
            {
                Vector2 viewedChunkCoord = new Vector2(currentChunkCoordX + xOffset, currentChunkCoordY + yOffset);

                if (chunkDictionary.ContainsKey(viewedChunkCoord))
                {
                    chunkDictionary[viewedChunkCoord].UpdateMeshChunk();
                    if (chunkDictionary[viewedChunkCoord].isVisible())
                    {
                        meshChunksVisibleLastUpdate.Add(chunkDictionary[viewedChunkCoord]);
                    }
                }
                else
                {
                    chunkDictionary.Add(viewedChunkCoord, new MeshChunk(viewedChunkCoord, chunkSize, transform, mapMatrial));
                }
            }
        }
    }

    #endregion Private Methods

    public class MeshChunk
    {
        private GameObject meshObject;
        private Vector2 position;
        private Bounds bounds;

        private MapData mapData;

        private MeshRenderer meshRenderer;
        public MeshFilter meshFilter;

        public MeshChunk(Vector2 coord, int size, Transform parent, Material material)
        {
            position = coord * size;
            bounds = new Bounds(position, Vector2.one * size);
            Vector3 positionV3 = new Vector3(position.x, 0, position.y);

            meshObject = new GameObject("Mesh Chunk");
            meshRenderer = meshObject.AddComponent<MeshRenderer>();
            meshFilter = meshObject.AddComponent<MeshFilter>();
            meshRenderer.material = material;

            meshObject.transform.position = positionV3;
            meshObject.transform.parent = parent;
            SetVisible(false);

            mapGenerator.RequestMapData(position, OnMapDataRecieved);
        }

        private void OnMapDataRecieved(MapData mapData)
        {
            mapGenerator.RequestMeshData(mapData, OnMeshDataRecieved);
        }

        private void OnMeshDataRecieved(MeshData meshData)
        {
            meshFilter.mesh = meshData.CreateMesh();
        }

        public void UpdateMeshChunk()
        {
            float viewerDistFromNearestEdge = Mathf.Sqrt(bounds.SqrDistance(viewerPosition));
            bool visible = (viewerDistFromNearestEdge <= maxViewDistance);
            SetVisible(visible);
        }

        public void SetVisible(bool visible)
        {
            meshObject.SetActive(visible);
        }

        public bool isVisible()
        {
            return meshObject.activeSelf;
        }
    }
}