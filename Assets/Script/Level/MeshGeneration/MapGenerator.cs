﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Lukas Moungos
 */

public class MapGenerator : MonoBehaviour
{
    #region Public Fields

    [SerializeField]
    public static int chunkSize = 50;

    [Range(0.1f, 500f)]
    public float scale;

    [Range(0.1f, 500f)]
    public float heightMultiplier;

    public Noise.NormilizeMode normilizeMode;

    public int octaves;
    public float persistance;
    public float lacunarity;
    public int seed;
    public Vector2 offset;

    #endregion Public Fields

    #region Private Fields

    private Queue<MapThreadInfo<MapData>> mapDataInfoQueue = new Queue<MapThreadInfo<MapData>>();
    private Queue<MapThreadInfo<MeshData>> meshDataInfoQueue = new Queue<MapThreadInfo<MeshData>>();

    #endregion Private Fields

    #region Unity Methods

    // Update is called once per frame
    private void Update()
    {
        if (mapDataInfoQueue.Count > 0)
        {
            for (int i = 0; i < mapDataInfoQueue.Count; i++)
            {
                MapThreadInfo<MapData> threadInfo = mapDataInfoQueue.Dequeue();
                threadInfo.callback(threadInfo.parameter);
            }
        }

        if (meshDataInfoQueue.Count > 0)
        {
            for (int i = 0; i < meshDataInfoQueue.Count; i++)
            {
                MapThreadInfo<MeshData> threadInfo = meshDataInfoQueue.Dequeue();
                threadInfo.callback(threadInfo.parameter);
            }
        }
    }

    #endregion Unity Methods

    #region Private Methods

    private MapData GenerateMapData(Vector2 centre)
    {
        float[,] heightMap = Noise.GenerateNoiseMap(chunkSize, seed, scale, octaves, persistance, lacunarity, centre + offset, normilizeMode);
        return new MapData(heightMap);
    }

    #endregion Private Methods

    public void RequestMapData(Vector2 centre, Action<MapData> callback)
    {
        ThreadStart threadStart = delegate
        {
            MapDataThread(centre, callback);
        };

        new Thread(threadStart).Start();
    }

    private void MapDataThread(Vector2 centre, Action<MapData> callback)
    {
        MapData mapData = GenerateMapData(centre);
        lock (mapDataInfoQueue)
        {
            mapDataInfoQueue.Enqueue(new MapThreadInfo<MapData>(callback, mapData));
        }
    }

    public void RequestMeshData(MapData mapData, Action<MeshData> callback)
    {
        ThreadStart threadStart = delegate
        {
            MeshDataThread(mapData, callback);
        };
        new Thread(threadStart).Start();
    }

    private void MeshDataThread(MapData mapData, Action<MeshData> callback)
    {
        MeshData meshData = MeshGenerator.GenerateMesh(mapData.heightMap, heightMultiplier);
        lock (meshDataInfoQueue)
        {
            meshDataInfoQueue.Enqueue(new MapThreadInfo<MeshData>(callback, meshData));
        }
    }

    private struct MapThreadInfo<T>
    {
        public readonly Action<T> callback;
        public readonly T parameter;

        public MapThreadInfo(Action<T> callback, T parameter)
        {
            this.callback = callback;
            this.parameter = parameter;
        }
    }

    public static class Noise
    {
        public enum NormilizeMode { Local, Global };

        public static float[,] GenerateNoiseMap(int chunkSize, int seed, float scale, int octaves, float persistance, float lacunarity, Vector2 offset, NormilizeMode normilizeMode)
        {
            float[,] noiseMap = new float[chunkSize + 1, chunkSize + 1];

            System.Random prng = new System.Random(seed);
            Vector2[] octaveOffsets = new Vector2[octaves];

            float maxPossibleHeight = 0;
            float amplitude = 1;
            float frequency = 1;

            for (int i = 0; i < octaves; i++)
            {
                float offsetX = prng.Next(-100000, 100000) + offset.x;
                float offsetY = prng.Next(-100000, 100000) + offset.y;
                octaveOffsets[i] = new Vector2(offsetX, offsetY);

                maxPossibleHeight += amplitude;
                amplitude *= persistance;
            }

            if (scale <= 0)
            {
                scale = 0.0001f;
            }

            float maxLocalHeight = float.MinValue;
            float minLocalHeight = float.MaxValue;

            float halfWidth = chunkSize / 2f;
            float halfHeight = chunkSize / 2f;

            for (int y = 0; y <= chunkSize; y++)
            {
                for (int x = 0; x <= chunkSize; x++)
                {
                    float noiseHeight = 0;
                    amplitude = 1;
                    frequency = 1;

                    for (int i = 0; i < octaves; i++)
                    {
                        float sampleX = (x - halfWidth + octaveOffsets[i].x) / scale * frequency;
                        float sampleY = (y - halfHeight + octaveOffsets[i].y) / scale * frequency;

                        float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
                        noiseHeight += perlinValue * amplitude;

                        amplitude *= persistance;
                        frequency *= lacunarity;
                    }
                    if (noiseHeight > maxLocalHeight)
                    {
                        maxLocalHeight = noiseHeight;
                    }
                    else if (noiseHeight < minLocalHeight)
                    {
                        minLocalHeight = noiseHeight;
                    }

                    noiseMap[x, y] = noiseHeight;
                }
            }
            for (int y = 0; y <= chunkSize; y++)
            {
                for (int x = 0; x <= chunkSize; x++)
                {
                    if (normilizeMode == NormilizeMode.Local)
                    {
                        noiseMap[x, y] = Mathf.InverseLerp(minLocalHeight, maxLocalHeight, noiseMap[x, y]);
                    }
                    else
                    {
                        float normalizedHeight = (noiseMap[x, y] + 1) / (2f * maxPossibleHeight / 1.75f);
                        noiseMap[x, y] = Mathf.Clamp(normalizedHeight, 0, int.MaxValue);
                    }
                }
            }

            return noiseMap;
        }
    }
}

public struct MapData
{
    public readonly float[,] heightMap;

    public MapData(float[,] heightMap)
    {
        this.heightMap = heightMap;
    }
}