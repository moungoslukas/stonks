﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEditor;
using UnityEngine;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Lukas Moungos
 */

public static class MeshGenerator
{
    public static MeshData GenerateMesh(float[,] heightMap, float heightMultiplier)
    {
        int xSize = heightMap.GetLength(0);
        int zSize = heightMap.GetLength(1);

        MeshData meshData = new MeshData(xSize, zSize);
        int vertIndex = 0;

        for (int z = 0; z < zSize; z++)
        {
            for (int x = 0; x < xSize; x++)
            {
                meshData.vertices[vertIndex] = new Vector3(x, heightMap[x, z] * heightMultiplier, z);
                vertIndex++;
            }
        }
        meshData.InstatiateTriangles();
        return meshData;
    }
}

public class MeshData
{
    public Vector3[] vertices;
    public int[] triangles;
    public Vector2[] uvs;
    public int meshWidth;
    public int meshHeight;

    public MeshData(int meshWidth, int meshHeight)
    {
        vertices = new Vector3[(meshWidth + 1) * (meshHeight + 1)];
        triangles = new int[meshWidth * meshHeight * 6];
        uvs = new Vector2[vertices.Length];
        this.meshWidth = meshWidth - 1;
        this.meshHeight = meshHeight - 1;
    }

    private Vector3[] CalculateNormals()
    {
        Vector3[] vertexNormals = new Vector3[vertices.Length];
        int trianglesCount = triangles.Length / 3;
        for (int i = 0; i < trianglesCount; i++)
        {
            int normalTriangleIndex = i * 3;
            int vertexIndexA = triangles[normalTriangleIndex];
            int vertexIndexB = triangles[normalTriangleIndex + 1];
            int vertexIndexC = triangles[normalTriangleIndex + 2];

            Vector3 triangleNormal = SurfaceNormalFromIndecies(vertexIndexA, vertexIndexB, vertexIndexC);
            vertexNormals[vertexIndexA] += triangleNormal;
            vertexNormals[vertexIndexB] += triangleNormal;
            vertexNormals[vertexIndexC] += triangleNormal;
        }

        for (int i = 0; i < vertexNormals.Length; i++)
        {
            vertexNormals[i].Normalize();
        }
        return vertexNormals;
    }

    private Vector3 SurfaceNormalFromIndecies(int IndexA, int IndexB, int IndexC)
    {
        Vector3 pointA = vertices[IndexA];
        Vector3 pointB = vertices[IndexB];
        Vector3 pointC = vertices[IndexC];

        Vector3 sideAB = pointB - pointA;
        Vector3 sideAC = pointC - pointA;
        return Vector3.Cross(sideAB, sideAC).normalized;
    }

    public Mesh CreateMesh()
    {
        Mesh mesh = new Mesh();
        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;
        mesh.uv = uvs;
        mesh.normals = CalculateNormals();
        return mesh;
    }

    public void InstatiateTriangles()
    {
        int vert = 0;
        int tris = 0;

        for (int z = 0; z < meshHeight; z++)
        {
            for (int x = 0; x < meshWidth; x++)
            {
                triangles[tris + 0] = vert + 0;
                triangles[tris + 1] = vert + meshWidth + 1;
                triangles[tris + 2] = vert + 1;
                triangles[tris + 3] = vert + 1;
                triangles[tris + 4] = vert + meshWidth + 1;
                triangles[tris + 5] = vert + meshWidth + 2;

                vert++;
                tris += 6;
            }
            vert++;
        }

        for (int i = 0; i < uvs.Length; i++)
        {
            uvs[i] = new Vector2(vertices[i].x, vertices[i].z);
        }
    }
}