﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Lukas Moungos
 */

public class Boundary : MonoBehaviour
{
    private void OnTriggerExit(Collider other)
    {
        Destroy(other.gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != "PivkupItem") return;
        Destroy(collision.gameObject);
    }
}