﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Lukas Moungos
 */

public class ParticleDestroy : MonoBehaviour
{
    #region Unity Methods

    // Start is called before the first frame update
    private void Start()
    {
        ParticleSystem ps = GetComponentInChildren<ParticleSystem>();

        var main = ps.main;

        ps.Play();
        Destroy(gameObject, main.duration);
    }

    #endregion Unity Methods
}