﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Lukas Moungos
 */

public class ObjectSpawner : MonoBehaviour
{
    private Vector2 spawnPoint { get { return FindRandomPointInSquare(); } }
    public Difficulty difficulty;
    public int maxGameObjectsInScreen;

    [System.Serializable]
    public class Scenery
    {
        public GameObject prefab;
        public Vector3 spawnOffset;
    }

    public Scenery scenery;

    [System.Serializable]
    public class Spawnable
    {
        public GameObject prefab;
        [ReadOnly] [Range(0f, 1f)] public float spawnChance = 0.5f;
        [Range(0f, 1f)] public float maximunSpawnChance = 1;
        [Range(0f, 1f)] public float minimumSpawnChance = 0;
        public float offset;
    }

    [Space(10)]
    public GameObject bossObject;

    public float bossIntervall;
    private float bossTimer;
    [Range(0.00f, 1.00f)] public float bossSpawnChance;

    [Space(10)]
    public bool spawnEnemies = true;

    public float spawnRange;

    public Spawnable[] spawnables;

    private GameObject spawnableObjectsHolder;
    private GameObject lastObject;

    [Space(10)]
    public float difficultyScaleTimer;

    [Range(0.00f, 1.00f)] public float difficultyMultiplier;
    public float minimumSpawnRange;
    public float spawnRangeDecreement;
    private float timer;

    #region Unity Methods#

    private void Start()
    {
        spawnableObjectsHolder = GameObject.FindGameObjectWithTag("Spawnables");
        if (spawnableObjectsHolder == null)
            spawnableObjectsHolder = new GameObject("Spawnables");

        timer = difficultyScaleTimer;
        bossTimer = bossIntervall;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag != "Scenery") return;
        Instantiate(scenery.prefab, transform.position - scenery.spawnOffset, transform.rotation);
    }

    private void OnValidate()
    {
        foreach (var s in spawnables)
        {
            if (s.spawnChance > s.maximunSpawnChance)
                s.spawnChance = s.maximunSpawnChance;
            if (s.spawnChance < s.minimumSpawnChance)
                s.spawnChance = s.minimumSpawnChance;
        }
    }

    // Update is called once per frame
    private void Update()
    {
        if (!spawnEnemies) return;
        if (Time.timeScale != 1) return;

        bossTimer -= Time.deltaTime;
        if (bossTimer <= 0) SpawnBoss();

        timer -= Time.deltaTime;
        if (timer <= 0) IncreaseDifficulty();

        if (spawnableObjectsHolder.transform.childCount >= maxGameObjectsInScreen) return;
        foreach (var s in spawnables)
        {
            float chance = Random.Range(0.01f, 1);
            if (chance > 1 - s.spawnChance)
            {
                Vector2 randomPoint = spawnPoint;
                if (lastObject != null && lastObject.transform.position.z - randomPoint.y < spawnRange) return;
                if (Physics.OverlapSphere(new Vector3(randomPoint.x, transform.position.y, randomPoint.y), spawnRange).Length > 0) return;
                GameObject obj = Instantiate(s.prefab);
                obj.transform.position = new Vector3(randomPoint.x, transform.position.y + s.offset, randomPoint.y);
                obj.transform.SetParent(spawnableObjectsHolder.transform);
                lastObject = obj;
                NormalizeSpawnchance(s);
                break;
            }
        }
    }

    private void LateUpdate()
    {
        foreach (var s in spawnables)
        {
            if (s.spawnChance > s.maximunSpawnChance)
                s.spawnChance = s.maximunSpawnChance;
            if (s.spawnChance < s.minimumSpawnChance)
                s.spawnChance = s.minimumSpawnChance;
        }
    }

    #endregion Unity Methods#

    private Vector2 FindRandomPointInSquare()
    {
        float x = Random.Range(-3.5f, 3.5f);
        float y = Random.Range(transform.position.z, transform.position.z - 100);

        return new Vector2(x, y);
    }

    private void NormalizeSpawnchance(Spawnable spawnable)
    {
        float precantageDelta = 0;
        foreach (var s in spawnables)
        {
            if (s == spawnable)
            {
                precantageDelta = s.spawnChance * 0.02f;
                s.spawnChance -= precantageDelta;
                continue;
            }
            s.spawnChance += precantageDelta / (spawnables.Length - 2);
        }
    }

    private void IncreaseDifficulty()
    {
        foreach (var s in spawnables)
        {
            s.minimumSpawnChance = s.minimumSpawnChance != 1f ? s.minimumSpawnChance + difficultyMultiplier : 1;
            s.maximunSpawnChance = s.maximunSpawnChance != 1f ? s.maximunSpawnChance + difficultyMultiplier : 1;
        }
        spawnRange = spawnRange > minimumSpawnRange ? spawnRange - spawnRangeDecreement : minimumSpawnRange;
        timer = difficultyScaleTimer;
    }

    private void SpawnBoss()
    {
        if (bossSpawnChance > 1 - Random.value)
        {
            Instantiate(bossObject, transform.position, transform.rotation);
            spawnEnemies = false;
            bossTimer = bossIntervall;
        }
    }
}

public enum Difficulty
{
    Easy,
    Medium,
    Hard
}