﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* 
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Alexander Herrmann
 */

public class HealthBar : MonoBehaviour
{
    #region Public Fields
    public float barDisplay;
    public Vector2 position = new Vector2(20, 40);
    public Vector2 size = new Vector2(70, 20);
    public Texture2D emptyBar;
    public Texture2D filledBar;
    #endregion
    void Start()
    {
        stats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
    }
    #region Private Fields
    PlayerStats stats;
    #endregion

    #region Unity Methods

    void OnGUI()
    {
        GUI.BeginGroup(new Rect(position.x, position.y, size.x, size.y));
        GUI.Box(new Rect(0, 0, size.x, size.y), emptyBar);


        GUI.BeginGroup(new Rect(0, 0, size.x * barDisplay, size.y));
        GUI.Box(new Rect(0, 0, size.x, size.y), filledBar);
        GUI.EndGroup();
        GUI.EndGroup();
    }

    void Update()
    {
        barDisplay = stats.health*0.01f;
    }
    #endregion
}