﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/* 
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Alexander Herrmann
 */

public class ScoreUI : MonoBehaviour
{
    #region Public Fields

    #endregion

    #region Private Fields
    private Text text;
    private PlayerStats stats;
    #endregion

    #region Unity Methods

    // Start is called before the first frame update
    private void Start()
    {
        stats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (text.text == null)
        {
            stats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
            return;
        }
        text.text = stats.score.ToString();
    }

    // LateUpdate is called after all Update functions have been called.
    void LateUpdate()
    {
        
    }
    #endregion
 
    #region Private Methods
    
    #endregion
  
}