﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/* 
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Alexander Herrmann
 */

public class LoadingSlider : MonoBehaviour
{
    #region Public Fields

    #endregion

    #region Private Fields
    private Slider slider;
    private float TimeOfNextAction;
    #endregion

    #region Unity Methods
 
    // Start is called before the first frame update
    void Start()
    {
        slider = GetComponent<Slider>();
        TimeOfNextAction = 0f;
        slider.value = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > TimeOfNextAction)
        {
            TimeOfNextAction += 0.01f;
            slider.value += 0.001f;
        }
    }
 	
    // LateUpdate is called after all Update functions have been called.
    void LateUpdate()
    {
        
    }
    #endregion
 
    #region Private Methods
    
    #endregion
  
}