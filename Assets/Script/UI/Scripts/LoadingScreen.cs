﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/* 
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Alexander Herrmann
 */

public class LoadingScreen : MonoBehaviour
{
    #region Public Fields

    #endregion

    #region Private Fields
    private float time;
    #endregion

    #region Unity Methods

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LoadAsyncOperation());
    }
    IEnumerator LoadAsyncOperation()
    {
        yield return new WaitForSeconds(10);
        AsyncOperation gameLevel = SceneManager.LoadSceneAsync(2);      
    }

    // Update is called once per frame
    void Update()
    {

    }
 	
    // LateUpdate is called after all Update functions have been called.
    void LateUpdate()
    {
        
    }
    #endregion
 
    #region Private Methods
    
    #endregion
  
}