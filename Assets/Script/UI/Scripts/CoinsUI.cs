﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Alexander Herrmann
 */

public class CoinsUI : MonoBehaviour
{
    #region Private Fields

    private Text text;
    private PlayerStats stats;

    #endregion Private Fields

    #region Unity Methods

    // Start is called before the first frame update
    private void Start()
    {
        stats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (text.text == null)
        {
            stats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
            return;
        }
        text.text = stats.coins.ToString();
    }

    // LateUpdate is called after all Update functions have been called.
    private void LateUpdate()
    {
    }

    #endregion Unity Methods
}