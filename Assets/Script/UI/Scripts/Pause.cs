﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* 
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Alexander Herrmann
 */

public class Pause : MonoBehaviour
{
    public GameObject PauseMenu;
    private AudioManger audioManager;
    private void Start()
    {
        audioManager = FindObjectOfType<AudioManger>();
    }
    public void PauseGame()
    {
        if (Time.timeScale <= 0)
        {
            Time.timeScale = 1;
            PauseMenu.SetActive(false);
            audioManager.AddSound("Theme", 0.045f);
        }
        else
        {
            Time.timeScale = 0;
            PauseMenu.SetActive(true);
            audioManager.SubSound("Theme", 0.045f);
        }
    }
}