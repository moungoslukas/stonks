﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/* 
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Alexander Herrmann
 */

public class HighScoreUI : MonoBehaviour
{
    private Text text ;
    private void Start()
    {       
        text = GetComponent<Text>();
    }
    private void Update()
    {
        text.text = PlayerPrefs.GetInt("Highscore").ToString();      
    }
}