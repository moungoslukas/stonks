﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/* 
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Alexander Herrmann
 */

public class MainMenu : MonoBehaviour
{
    public GameObject camera;
    public Vector3 currentAngle;
    public Vector3 targetAngle = new Vector3(0f,90f,0f);
    public GameObject ShipSelectionUI;
    private float speed = 0.05f;
    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void EnterShipSelection()
    {
        currentAngle = camera.transform.eulerAngles;
        InvokeRepeating("LerpRotation", 0, Time.deltaTime);
        ShipSelectionUI.SetActive(true);
    }
    public void LerpRotation()
    {
        currentAngle = new Vector3
            (
             Mathf.LerpAngle(currentAngle.x, targetAngle.x, speed),
             Mathf.LerpAngle(currentAngle.y, targetAngle.y, speed),
             Mathf.LerpAngle(currentAngle.z, targetAngle.z, speed)
             );
        camera.transform.eulerAngles = currentAngle;
    }

}