﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * Copyright (c) 2020 All Rights Reserved
 *  @Author: Alexander Herrmann
 */

public class HealthBarUI : MonoBehaviour
{
    #region Public Fields

    public Image fillimage;
    private Text text;

    #endregion Public Fields

    #region Private Fields

    private PlayerStats stats;
    private Slider slider;

    #endregion Private Fields

    #region Unity Methods

    // Start is called before the first frame update
    private void Start()
    {
        stats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStats>();
        slider = GetComponent<Slider>();
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (slider.value <= slider.minValue)
        {
            fillimage.enabled = false;
        }
        else
        {
            fillimage.enabled = true;
        }
        text.text = stats.health.ToString();
        float fillvalue = stats.health / 100f;
        slider.value = fillvalue;
    }

    // LateUpdate is called after all Update functions have been called.
    private void LateUpdate()
    {
    }

    #endregion Unity Methods
}